/** 
 * 블록체인을 구현하고
 * 작업증명(채굴)을 하여 블록을 생성하는 예제입니다.
 * 
 * 최종 수정일: 2018/05/02
*/

/* 암호화를 위한 모듈 */
const crypto = require('crypto');

class BlockChain {
  constructor(version) {
    this.blockchain = new Array(); // 빈 배열 생성 
    this.transactions = new Array();
    this.version = version;
    this.bits = 4; // 난이도 4로 설정(해쉬값의 맨 앞~4번째 문자가 0 이어야 함)
    this.new_block = new Block(this.blockchain.length, this.version, "0", this.bits, 0, this.transactions, "Genesis Block"); // Genesis 블록 생성(첫 블록)
    this.blockchain.push(this.new_block); // 새 블록 생성 및 블록체인에 추가 
  }

  /**
   * 새로운 트랜잭션(거래) 추가
   * 
   * @param {string} sender 보내는 사람
   * @param {string} reciver 받는 사람
   * @param {number} amount 거래 양(코인 수)
   */
  new_transaction(sender, reciver, amount) {
    this.transactions.push({
      'sender': sender,
      'reciver': reciver,
      'amount': amount
    });
  }

  /**
   * 블록 생성(체인에 반영 X)
   * 
   * @param {string} prev_hash 이전 블록의 해쉬값
   * @param {Array<any>} prev_transactions 이전 블록의 트랜잭션
   * @param {string} data 블록에 저장할 데이터
   */
  create_block(prev_hash, prev_transactions, data) {
    this.new_block = new Block(this.blockchain.length, this.version, prev_hash, this.bits, 0, prev_transactions.concat(this.transactions), data);
  }
  
  /**
   * 생성된 블록 체인에 추가
   */
  add_block() {
    this.blockchain.push(this.new_block);
    this.transactions = [];
  }

  /**
   * 이전 블록
   */
  prev_block() {
    return this.blockchain[this.blockchain.length - 1];
  }

  /**
   * 채굴 시작(작업증명 시작)
   * 
   * @param {string} data 블럭에 저장할 데이터
   */
  mine(data) {
    const prev_block = this.prev_block();
    const prev_hash = prev_block.hash;
    const prev_transactions = prev_block.transactions;
    this.new_transaction('system', 'me', 1); // 블록체인 시스템이 나에게 1개의 코인 지급(작업증명 성공 시 보상)
    this.create_block(prev_hash, prev_transactions, data);
    this.pow();
    this.add_block();
  }

  /**
   * Proof of work
   * 작업증명
   */
  pow() {
    let nonce = 0; // 0 ~ 조건에 맞을 때 까지반복
    while(!this.validNonce(nonce)) {
      nonce++;
    }
  }

  /**
   * 해당 난스로 적용한 후 해싱하였을 때 조건에 맞는지 확인
   * 
   * @param {number} nonce 난스
   */
  validNonce(nonce) {
    const sha = crypto.createHash('sha256');
    this.new_block.header.nonce = nonce // 난스 설정 
    const s = JSON.stringify(this.new_block.header); // 블록 헤더 암호화
    sha.update(s);
    const hash = sha.digest('hex');
    console.log(hash, nonce);
    for(let i=0; i<this.bits; i++) { // 첫~bits 자리의 데이터가 모두 0인지 비교
      if(hash[i] != '0') { // 만약 0이 아닌것이 있으면 false
        return false;
      }
    }
    this.new_block.hash = hash; // 블록 해시 설정
    return true;
  }
}

/**
 * 블록 헤더객체
 * version: 블록체인 버전
 * prev_hash: 이전 블록 해쉬
 * merkle_hash: 트랜잭션 해쉬
 * time: 블록 생성시간
 * bits: 해쉬 앞 0 갯수(난이도)
 * nonce: 조건에 맞는 해쉬값(채굴) 
 */
class BlockHeader {
  constructor(version, prev_hash, merkle_hash, time, bits, nonce) {
    this.version = version;
    this.prev_hash = prev_hash;
    this.merkle_hash = merkle_hash;
    this.time = time;
    this.bits = bits;
    this.nonce = nonce;
  }
}

/**
 * 블록 객체
 * index: 순번
 * version: 블록체인 버전
 * prev_hash: 이전 블록 해쉬
 * bits: 해쉬 앞 0 갯수(난이도)
 * nonce: 조건에 맞는 해쉬값(채굴)
 * transactions: 거래내역
 * data: 블록에 저장될 데이터
 */
class Block {
  constructor(index, version, prev_hash, bits, nonce, transactions, data) {
    this.index = index;
    this.transactions = transactions;
    this.data = data;
    this.header = new BlockHeader(version, prev_hash, this.hash(this.transactions), Date.now(), bits, nonce);
    this.hash = '0' // 현재 블럭의 해쉬값
  }

  hash(data) {
    const sha = crypto.createHash('sha256');
    sha.update(JSON.stringify(data));
    return sha.digest('hex');
  }
}

// 블록체인 생성
blockchain = new BlockChain('1.0'); // 버전은 1.0으로 지정

blockchain.mine('Hello world'); // 채굴시작(작업증명)
blockchain.mine('Mining!!!');
blockchain.mine('반갑습니다^^'); // 총 3개의 블럭 추가

console.dir(blockchain.blockchain); // Genesis 블럭 + 작업증명된 블럭 3개 = 총 4개 블럭 출력
console.log('\n\n');

// 트랜잭션 출력
for(let block of blockchain.blockchain) {
  console.log(`#${block.index} 블록에 저장되어있는 트랜잭션(거래내역)`);
  for(let t of block.transactions) {
    console.log(t);
  }
  console.log('====================================');
}