/** 
 * 간단한 블록을 구현하고
 * 예제를 통해 블록체인에 대해
 * 쉽게 이해할 수 있습니다. 
 * 
 * 블록은 이전 블록의 해쉬값(암호화 값)을 가지고 있고
 * 블록번호와 블록에 저장될 데이터, 시간 등의 데이터를 가지고 있습니다.
 * 
 * 블록 해쉬값은 블록 데이터(블록번호, 데이터, 시간 등)모두를 통합한 데이터를 암호화합니다.
 * 
 * 최종 수정일: 2018/05/02
*/

/* 암호화를 위한 모듈 */
const crypto = require('crypto');

/**
 * 블록 객체
 * index: 순번
 * time: 생성된 시간
 * data: 블록에 저장될 데이터
 * prev_hash: 이전 블록 해쉬
 * hash: 본 블록의 해쉬
 */
class Block {
  constructor(index, data, prev_hash) {
    this.index = index;
    this.time = Date.now();
    this.data = data;
    this.prev_hash = prev_hash;
    this.hash = this.hash();
  }

  hash() {
    const sha = crypto.createHash('sha256');
    sha.update(
      this.index +
      this.time +
      this.data +
      this.prev_hash
    );
    return sha.digest('hex'); 
  }
}

/**
 * 블록체인의 첫 번째 블록 생성
 */
const create_genesis_block = () => {
  return new Block(0, "Genesis block", "0"); 
}

/**
 * 블록을 저장할 배열 생성
 * (블록체인)
 * 
 * 배열의 첫 번째는 Genesis 블록
 */
let blockchain = new Array(create_genesis_block());
let prev_block = blockchain[0];


/**
 * 30개의 추가 블록 생성
 */
for(let i = 1; i<=30; i++) {
  let new_block_index = prev_block.index + 1;
  let new_block_data = `${i}번 블록!!`;
  let new_block_prev_hash = prev_block.hash;

  // 신규블록 생성
  let new_block = new Block(new_block_index, new_block_data, new_block_prev_hash);
  blockchain.push(new_block);
  prev_block = new_block;
  for(let j=0; j<10000000; j++); // 약간 시간을 지연시키기 위한 로직
}

/**
 * 블록체인에 있는 블록 확인(출력)
 */
for(let block of blockchain) {
  console.log(`#${block.index} Block`);
  console.log(`Time: ${block.time}`);
  console.log(`Data: ${block.data}`);
  console.log(`Previous hash: ${block.prev_hash}`);
  console.log(`Hash: ${block.hash}\n`);
}